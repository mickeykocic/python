#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

from sys import exit

class LogicGate:

    def __init__(self, name):
        self.name = name
        
    def set_pin_name(self, name):
        self.name = name
        
    def set_pin(self, pinsetting, pinname='N'):
        if int(pinsetting) !=0 and int(pin) != 1:
            print("Pin must be 0 or 1.")
            exit()
        else:
            self.pinsetting = pinsetting
            return self.pinsetting
        
        
        
class BinaryGate(LogicGate):

    def __init__(self, name):
        LogicGate.__init__(self, name)
        
    def set_pin_A(self, pinsetting):
        self.pinA = set_pin(self, pinsetting, 'A')
        
    def set_pin_B(self, pinsetting):
        self.PinB = set_pin(self, pinsetting, 'B')
        
        
class AndGate(BinaryGate):

    def__init__(self, name):
        LogicGate.__init__(self, name)
        
    def set_pins(self, pinsetting1, pinsetting2):
        self.pinA = set_pin_A(self, pinsetting)
        self.pinB = set_pin_B(self, pinsetting)
        
    def set_output(self):
        if int(self.pinA) == 1 and int(self.pinB) == 1:
            self.output = 1
        else:
            self.output = 0
