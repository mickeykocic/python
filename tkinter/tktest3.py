#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

from tkinter import *

class MyApp:

    def __init__(self, myParent):
    
        self.myContainer1 = Frame(myParent)
        self.myContainer1.pack()
        
        self.button1 = Button(self.myContainer1)
        self.button1["text"] = "This is a somewhat different piece of text."
        self.button1["background"] = "lightgreen"
        self.button1.pack()
        
        self.button2 = Button(self.myContainer1)
        self.button2.configure(text="And this is still more text.")
        self.button2.configure(background="lightblue")
        self.button2.pack()
        
        self.button3 = Button(self.myContainer1)
        self.button3.configure(text="Finally, some text.", background="yellow")
        self.button3.pack()
        
        self.button4 = Button(self.myContainer1, text="See ya!", background="orange")
        self.button4.pack()
        
        
root = Tk()
myapp = MyApp(root)
root.mainloop()
