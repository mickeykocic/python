#! /usr/bin/env python3
# -*- encoding: utf8 -*-

from tkinter import *

root = Tk()

myContainer1 = Frame(root)
myContainer1.pack()

button1 = Button(myContainer1)
button1["text"] = "This is a test.\nDo not adjust your jockstrap\nThis is only a test."
button1["background"] = "yellow"
button1.pack()

root.mainloop()
