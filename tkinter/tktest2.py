#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

from tkinter import *

class MyApp:

    def __init__(self, myParent):
    
        self.myContainer1 = Frame(myParent)
        self.myContainer1.pack()
        
        self.button1 = Button(self.myContainer1)
        self.button1["text"] = "This is a somewhat different piece of text."
        self.button1["background"] = "lightgreen"
        self.button1.pack()
        
root = Tk()
myapp = MyApp(root)
root.mainloop()
