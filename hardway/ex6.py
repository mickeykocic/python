# -*- coding: utf-8 -*-

# assign a string to a variable with string formatting of integer
x = "There are %d types of people." % 10
# assign string to variable
binary = "binary"
# same as last line
do_not = "don't"
# assign string to variable with string formatting using two other string variables
y = "Those who know %s and those who %s." % (binary, do_not)

# print string through variable call
print x
# print string through variable call
print y

# print string literal with formatting using string variable
print "I said: %r." % x
# same as above
print "I also said: '%s'." % y

# assign boolean value to variable
hilarious = False
# assign string variable with string formatting using above boolena variable
joke_evaluation = "Isn't that joke so funy?! %r"

# print string variable with formatting using boolean variable
print joke_evaluation % hilarious

# the next two lines assign two string literals to variables
w = "This is the left side of..."
e = "a string with a right side."

# print a concatenation of two string literals through variable calls
print w + e
