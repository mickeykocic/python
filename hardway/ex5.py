# -*- coding: utf-8 -*-

name = 'Zed A. Shaw'
age = 35 # not a lie
height = 74 # inches
height_in_cm = round(74 * 2.54)
weight = 180 # lbs
weight_in_kg = round(180 * 0.453592)
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

print "Let's talk about %s." % name
print "He's %d inches tall, which is %d in centimetres." % (height, height_in_cm)
print "He's %d pounds heavy, which is %d in kilograms." % (weight, weight_in_kg)
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee." % teeth

# this line is tricky, try to get it exactly right
print "If I add %d, %d, and %d I get %d." % (
    age, height, weight, age + height + weight)
