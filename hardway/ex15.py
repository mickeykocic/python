# -*- coding: utf-8 -*-

# import argv from sys module
from sys import argv

# identify command line arguments, including the script filename
script, filename = argv

# use filename from command line to open file
txt = open(filename)

# print notification that the file will be read and then read it into STDOUT
print "Here's your file %s." % filename
print txt.read()

# close file
txt.close()

# prompt for filename using raw_input and then get filename
print "Type the filename again:"
file_again = raw_input('> ')

# open file as named in response to raw_input prompt
txt_again = open(file_again)

# read file contents into STDOUT
print txt_again.read()

# close file
txt_again.close()
