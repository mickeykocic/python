# -*- coding: utf-8 -*-

print "This program will open and read a file, then erase it and write \
new material to it."
raw_input("Press enter when ready or ctrl-c to exit.")

filename = raw_input("What's the name of the file? ")
print "Opening file..."
txt = open(filename, 'w')
print "Erasing file..."
print "File is now empty."

print "Now type three lines."
line1 = raw_input("First line: ")
line2 = raw_input("Second line: ")
line3 = raw_input("Third line: ")

print "Formatting output..."

endl = "\n"
lines = line1 + endl + line2 + endl + line3 + endl

print "Writing to file..."
txt.write(lines)

print "Closing file..."
txt.close()


print "To read the new file press enter. To exit, press ctrl-c."
raw_input("> ")
txt = open(filename)
print(txt.read())
txt.close()

