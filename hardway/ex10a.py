# -*- coding: utf-8 -*-

tab = '\t'
nuline = '\n'
print "%sThis%sis%stabbed." % (tab, tab, tab)
print "%sThis%sis%snewlined." % (nuline, nuline, nuline)

n = nuline
h = '\t*'
i = "cat food"
j = "fishies"
k = "catnip"
l = "grass"

print "This is my list: %s %s %s %s %s %s %s %s %s %s %s %s" % (n, h, i, n, h, j, n, h, k, n, h, l)
