# -*- coding: utf-8 -*-

# prints a string
print "I will now count my chickens:"

# prints a string and the result of an arithmetical calculation
print "Hens", 25.0 + 30.0 / 6.0
# same as last line of code
print "Roosters", 100.0 - 25.0 * 3.0 % 4.0

# prints a string
print "Now I will count the eggs:"

# prints the result of an arithmetical calculation
print 3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0

# prints a string
print "Is it true that 3 + 2 < 5 - 7?"

# prints the result of a boolean evaluation involving arithmetic
print 3.0 + 2.0 < 5.0 - 7.0

# prints a string and the result of an arithmetical computation
print "What is 3 + 2?", 3.0 + 2.0
# same as the line immediately above
print "What is 5 - 7?", 5.0 - 7.0

# prints a string
print "Oh, that's why it's False."

# same as last line above
print "How about some more."

# prints a string and the result of a boolean evaluation
print "Is it greater?", 5.0 > -2.0
# same as last line above
print "Is it greater or equal?", 5.0 >= 2.0
# same as last line above
print "Is it less or equal?", 5.0 <= 2.0
