# -*- coding: utf-8 -*-

print "The food costs $4.99."
print "The tax is 13% so", 4.99 * 0.13
print "The total with tax is", 4.99 * 1.13
print "The 15% tip is", (4.99 * 1.13) * 0.15
print "The total with tip is", (4.99 * 1.13) * 1.15
print "The goddamned tax and tip add", (4.99 * 0.13) + (4.99 * 1.13) * 0.15, "to the bill!"
print "That's an extra", (1.13 * 1.15 - 1) * 100, "percent!"
