#! /usr/bin/env python
# -*- coding: utf-8 -*-

def itergame_forloop(top, inc):

    list = []
    
    for item in range(0, top, inc):
        print "Item is now %d" % item
        list.append(item)
        print "List is now", list
        print "Item is still %d" % item
        
    print "The numbers:"
    for item in list:
        print item
        
        
itergame_forloop(30, 4)
