# -*- coding: utf-8 -*-

# these three lines print string literals, with the one on line 5 also using string
# formatting using a string literal in single quotation marks
print "Mary had a little lamb."
print "Its fleece was white as %s." % 'snow'
print "And everywhere that mary went."
# uses string repetition to repeat a string 10 times
print "." * 10

# the next 11 lines each assign a string character to a variable name
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that comma at the end.   try removing it to see what happens
# these two lines concatenate two sets of strings and print them. The comma at the 
# end of the next line causes the command interpreter to print a space rather than
# a newline at the end of the concatenated string. The print commands use variables
# as defined above, each variable representing one character
print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 + end11 + end12
