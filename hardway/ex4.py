# -*- coding: utf-8 -*-

# assign variable to total number of cars
cars = 100
# assign variable to total number of people who can fit in each car
space_in_a_car = 4
# assign variable to total number of drivers
drivers = 30
# assign variable to total number of passengers
passengers = 90
# assign variable to calculation determining how many cars have no drivers
cars_not_driven = cars - drivers
# assign variable to number of cars by aliasing number of available drivers
cars_driven = drivers
# assign variable to total carpool capacity using number of available cars and
# how many people can fit in each car
carpool_capacity = cars_driven * space_in_a_car
# assign variable to calculated value of how many people need to be in each car
# in order for the available drivers and cars to transport everyone
average_passengers_per_car = passengers / cars_driven


print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars today."
print "We can transport", carpool_capacity, "people today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passengers_per_car, "in each car."
