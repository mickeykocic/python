#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

# This code snippet is a set of jail functions for user input. The code
# is mostly self-explanatory.

def int_input():
	'''Jail-functioned input of integers'''
	input_string = ''
	while not input_string:
		input_string = input("Type an integer and hit <enter>: ")
		try:
			input_int = int(input_string)
		except:
			print("You must type an integer.")
			input_string = ''
	return input_int

def float_input():
	'''Jail-functioned input of floats'''
	input_string = ''
	while not input_string:
		input_string = input("Type an integer or float and hit <enter>: ")
		try:
			input_float = float(input_string)
		except:
			print("You must type an integer or float.")
			input_string = ''
	return input_float

def bool_input():
	'''Jail-functioned input of bool values'''
	input_string = ''
	while not input_string:
		input_string = input("Type a bool value and hit <enter>: ")
		if input_string == "True":
			input_bool = True
		elif input_string == "False":
			input_bool = False
		else:
			print("You must type a bool value.")
			input_string = ''
	return input_bool
