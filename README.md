# README #

This repository is for my exercises and experimentation in python. I have completed the Runestone Interactive version of _How to Think Like a Computer Scientist_ and am currently on their _Problem-Solving with Algorithms and Data Structures_. I am also going through the exercises in _Python the Hard Way_.

The <hardway> subdirectory is for my _PtHW_ exercises. The <snippets> directory is for code snippets I create in the course of learning. The <in.progress> directory is me practising developing applications. The scripts in the latter two are heavily commented and mostly self-explanatory anyway.

The <projects> directory, recently added, is for coding project plans and actual code relating to them. This arose out of exercise 36 in _Python the Hard Way_, which asks the student to spend a week designing and implementing a text game.

The recently added <runestone> directory is for my exercises and sandboxing arising from Runestone Interactive's _Problem-Solving With Algorithms and Data Structures_ course. The <tkinter> directory is for exercises arising from _Thinking in Tkinter_.

You can contact me at mickey.kocic /at/ gmx.com.

Mickey