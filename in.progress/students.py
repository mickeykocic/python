#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

# This is my experimentation with creating a database infrastructure using a
# python 3 class definition. The snippet includes a function for automating
# the creation of a class object. This work is in progress. Much of it is
# self-explanatory but anyone with questions or comments can contact me at
# mickey.kocic /at/ gmx.com

class Student:
    """Create student class by using student number as identifier"""
    def __init__(self, number):
        self.number = number
        
    def name(self, surname, prename, middlenames=None):
        """Set student name"""
        self.surname = surname
        self.prename = prename
        self.middlenames = middlenames
        
    def age(self, age, bdate):
        """Set student age and birth date"""
        self.bdate = bdate
        self.age = age
        
    def role(self, grun):
        """Set boolean that returns True if undergrad and False if grad"""
        if grun:
            self.grun = "undergrad"
        else:
            self.grun = "graduate"
        
    def status(self, status):
        """Set standing with educational institution"""
        self.status = status
        
    def major(self, major):
        """Set name of major"""
        self.major = major
        
    def year(self, year):
        """Set year of study"""
        self.year = year
        
    def gpa(self, gpa):
        """Set current grade-point average"""
        self.gpa = gpa
        
def build_student():
    """This function automates the creation of a Student object"""
    
    # create student object
    a = Student(input("Student number?"))
    
    # add student name
    b = input("Name (no periods)?")
    b = b.split(" ")
    if len(b) > 3:
        c = str(b[1:len(b)-2])
    else:
        c = b[1]
    a.name(b[-1], b[0], c)
    
    # add student age and date of birth
    b = input("Age?")
    c = input("Date of birth (ddmmyy)?")
    a.age(b,c)
    
    #add role
    b = input("undergrad (y/n)?")
    if b == "y":
        a.role(True)
    elif b == "n":
        a.role(False)
    else:
        raise ValueError("Enter y or n only")
        
    # add status
    b = input("Enter status (good, probation, suspended).")
    a.status(b)
    
    # add major
    b = input("What major of study?")
    a.major(b)
    
    # add year
    b = input("What year of study?")
    a.year(b)
    
    # add grade-point average
    b = input("GPA?")
    a.gpa(b)

    # The return statement permits the assignment of a variable name to
    # the Student object    
    return a

