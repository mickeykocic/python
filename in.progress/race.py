#! /usr/bin/env Python3
# -*- encoding: utf-8 -*-

# This is a turtle-module race in which three differently-colored turtles race
# across the screen from left to right until one of them has "won" by going
# offscreen. This passive game is under development.


# check that user has tkinter installed
try:
    import turtle
except:
    raise ImportError "You do not have the tkinter module installed."
    quit()

import random

def dist(t):
    """Move each turtle forward a short, random distance."""
    t.forward(random.randrange(5,16))

def onscreen(t):
    """Verify whether winner has gone off canvas."""
    return abs(t.xcor()) < xw # see definition of xw below

window = turtle.Screen()
window.bgcolor("yellow")
xw = window.window_width()/2 # used in onscreen() function above

# use iteration to create three turtle objects and set colors and penup()
turt1, turt2, turt3 = turtle.Turtle(), turtle.Turtle(), turtle.Turtle()
for (x,y) in [(turt1,"blue"),(turt2,"red"),(turt3,"green")]:
    x.color(y)
    x.shape("turtle")
    x.penup()

# set starting positions at left edge of screen
turt1.goto(-xw+10, 20)
turt2.goto(-xw+10,0)
turt3.goto(-xw+10, -20)

# main procedural code for race. Each iteration causes only one of the
# three turtle objects to move forward.
while onscreen(turt1) and onscreen(turt2) and onscreen(turt3):
    x = random.randrange(0,3)
    if x == 0:
        dist(turt1)
    elif x == 1:
        dist(turt2)
    else:
        dist(turt3)

window.exitonclick()


