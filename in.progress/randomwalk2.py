#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

# This short turtle game has three turtle objects start in the middle of the
# canvas and move around randomly a la insects until they all go off the edge
# of the canvas. It is under development.

# Checks if user has tkinter installed.
try:
    import turtle
except:
    raise ImportError "You do not have tkinter installed."
    
import random

def isonscreen(t):
    """Check whether turtle object is still within the borders of the canvas."""
    x, y = t.xcor(), t.ycor()
    return abs(x) < xw and abs(y) < yw # see definition of xw/yw variable below

def randomwalk(t):
    """Determines turtle object's direction and movement distance randomly."""
    t.left(random.randrange(-180, 180))
    t.forward(random.randrange(25,76))

window = turtle.Screen()
window.bgcolor("yellow")

# Determines width of canvas for purposes of whether turtle is still on canvas.
xw, yw = window.window_width()/2, window.window_height()/2

# Creates three turtle objects and assigns attributes to them.
turt1, turt2, turt3 = turtle.Turtle(), turtle.Turtle(), turtle.Turtle()
for (x,y) in [(turt1, "blue"), (turt2, "red"), (turt3, "green")]:
    x.color(y)
    x.shape("turtle")
    x. penup()

# Main procedural routine for turtle random movement. First line initializes
# the while loop's terminating condition. Each selective branch terminates
# the script separately for that turtle.
z, a, b = True, True, True
while z or a or b:
    if isonscreen(turt1):
        randomwalk(turt1)
    else:
        z = False
    if isonscreen(turt2):
        randomwalk(turt2)
    else: 
        a = False
    if isonscreen(turt3):
        randomwalk(turt3)
    else:
        b = False

window.exitonclick()
